+++
title = "git show 2024"
date = "2025-01-13"
+++

> Ding-dong, c’est 2025 !

Le temps passe vite dis donc, et j’ai oublié de passer du temps ici. Quoi de mieux qu’un petit
récap’ de 2024 pour remettre le pied à l’étrier et recommencer à blablater sur ce blog ?

_Sur une idée pas très originale, piquée du formidable Siegfried que vous pouvez lire [ici](https://ehret.me/to-2025-and-beyond)._

## Le dev

### Côté pro

Début 2024, je quitte une scale-up désormais connue pour ses déboires malencontreux après un début prometteur (un peu comme la majorité des scale-up en somme).

Sans trop divulguer de détails, elle a, heureusement, été rachetée in-extremis ce qui évite à plusieurs
dizaines de personnes d’être sur le carreau. La magie de Noël j’imagine.

Fin Décembre, pour clôre une _longue_ recherche pour mon nouveau poste (dont j’ai résumé les grandes lignes [ici](/blogposts/une-annee-d-entretiens/))
je signe enfin 🎉

Ciao la morosité, et en route pour de nouvelles aventures [...].

Spoiler alert : c’était pas tout à fait ce qui me convenait.

<div style="width: 50%; margin: auto">

![Progressive disappointment](disappointment.gif)

</div>

En gros :

- je suis allergique à SCRUM à la sauce babysitting
- je suis encore plus allergique à la réunionite (conséquence de SCRUM évidemment)
- la moitié des presta devs sont remplacés 3 semaines après mon arrivée. Pas pratique pour créer du lien, surtout quand on est un poil introverti.
- les process pour du process, c’est légèrement agaçant
- ne faire que dépiler du ticket pour dépiler du ticket, ce n’est pas très stimulant

Bref, je passe sur les autres points. On ne peut pas tomber tout le temps dans des contextes épanouissants et c’est okay (tant que ça ne dure pas, j’imagine).
Ça peut convenir à d’autres, donc je ne vais pas jeter la pierre.

Me revoilà donc en train de prospecter ailleurs. Le marché n’est pas au beau fixe et si vous allez lu mon billet précédent, je redoutais un peu de
me réengager dans des process à rallonge.

> Et le voilà qui va nous pondre une suite 😑

J’aurais pu mais je vous rassure : cette fois-ci ma recherche ne m’a prise que quelques mois. Evidemment je suis tombé sur des cas _particuliers_
(faute d’autres termes) mais un des process qui n’a pas trop traîné s’est au final très bien passé.

En route pour de nouvelles aventures donc (bis) 🤞

### cd $HOME

J’ai lâché un peu de leste sur les projets perso. Non pas que j’en ai sorti beaucoup, mais cette année particulièrement,
je n’ai pas réussi à trouver assez de temps ni d’énergie pour coder des bêtises. Mais j’ai fait quelques trucs !

Bon j’avais pour projet de m’investir un peu plus sur LISP, une des dernières bizarreries avec laquelle je n’avais pas
encore joué, l’objectif étant d’essayer de faire l’Advent of Code avec. Autant dire que ce sera pour une autre fois, même si
c’était cool de se mettre à Emacs pendant quelques mois.

A la place, je me suis mis à .NET et C# (pour de futures raisons professionnelles). Pour le moment, c’est une
bouffée d’air frais (enfin, tout est relatif). Depuis bientôt 3 ans, je baigne dans l'univers Python quotidiennement.

Je n’ai pas spécifiquement de grief contre ce langage (je n’ai bossé que sur du CRUD classique), mais le tooling est une plaie constante.

Et devoir convaincre des gens d’utiliser des outils comme [ruff](https://docs.astral.sh/ruff/) ou [mypy](https://mypy-lang.org/) pour
avoir un dixième des garanties que j’aurais pu avoir sans effort avec Haskell est : fatigant. Surtout quand ces mêmes gens ne
s’y intéressent pas.

Sans tomber dans le trash-talk gratuit, la quantité de bugs débiles que j’ai pu corriger sur des codebase python est exaspérante.
Adieu les véléités de refactoring, parce que ça devient une tâche trop risquée.

**[mandatory rant about ORMs]**

Et je n’inclus pas les problèmes de perfs liés
à un usage qu’on qualifiera de naïf de SQLAlchemy (ou d’autre ORMs).

**[/mandatory rant about ORMs]**

Au final, j’ai fait la moitié de l’Advent of Code en C# et c’était chouette. Maintenant, l’objectif est de pouvoir
écrire des webservices classiques en .NET en y perdant le moins de cheveux possible.

En parlant d’Advent of Code, j’étais un poil plus relax que [l’an dernier](/blogposts/aoc-2023/).

Ce n’est pas grand chose, mais réussir à faire fonctionner son petit algo de pathfinding ou de programmation dynamique, et bien ça fait plaisir.

Les catégories de problèmes sont, au final, assez restreintes et à force de faire on choppe des automatismes.

Notons que ça ne me sert toujours absolument à rien dans mon quotidien, mais on cherche la stimulation où on peut !

Je ferai la deuxième moitié à l’occasion, si je trouve un peu de temps et d’envie.

Si au boulot, je me suis vite tourné vers VS Code (sans grande conviction), sur mon laptop perso je continue d’utiliser NeoVim (depuis pas mal d’années maintenant).

Et qui dit nouvelle année dit : réécrire sa config from scratch en se promettant d’avoir quelque chose de plus léger !

Je ne sais pas si le passage de VimScript à Lua a débloqué quelque chose, mais toucher à sa config est moins pénible.

La communauté et les plugins qui sortent me font aimer cet éditeur encore un peu plus, et je ne suis sans doute pas près de lâcher.

A côté de ça, un peu fatigué d’Alacritty, je suis passé sur WezTerm et forcément j’ai arrêté d’utiliser Zellij. Quant aux valeurs sûres, je garde
toujours dans un coin un Lazygit, ripgrep et un HTTPie (j’en ai sûrement oublié d’autres, mais j’aurai sûrement l’occasion d’en faire un billet).

## Le perso

Sans trop dévoiler de détails, 2024 a été riche en évènements.

Début d’année, je débute enfin mes cours de moto (alors que je n’ai jamais mis les pieds sur un deux roues motorisés) et je : souffre.

J’ai la chance de tomber dans une moto-école avec des profs super pédagogiques et une piste dédiée évitant ainsi un contexte
où on aurait été une quinzaine sur le plateau.

Hormis le code, il faut valider le plateau et la circulation. Le premier étant la partie souvent la plus difficile du permis.

_Une petite ellipse narrative_

Je passe le plateau en Juin, que j’obtiens du premier coup. Et la circulation est dans la poche, aussi du
premier coup, en Septembre.

Maintenant, c’est le moment d’angoisser à l’idée de rouler tout seul.

😬

J’ai désormais le loisir de pouvoir rouler et me faire doubler par des p*#t!ns de SUV alors que je roule
à 80 sur une roule à 80. Bref, malgré tous les inconvénients ça reste chouette.

A côté de ça, je me suis encore éloigné un peu plus de Paris pour profiter du calme (oui, ce n’est pas pour m’arranger avec
la diminution des postes en full-remote). Mes week-ends sont occupés à arpenter les allées des magasins de bricolage à la
recherche d’un truc (c’est le terme technique) qui me permettrait _enfin_ d’accrocher ce p*#t!n de luminaire, ou à m’instruire
sur la pose d’un parquet.

Je continue de grimper, moins qu’avant forcément. Je déserte petit à petit les salles de blocs, moins fan des ouvertures
et de l’ambiance générale (l’essor de ce sport est une bonne chose, mais pas sur tous les plans).

Du coup, je me fais les doigts et les avant-bras sur de la voie.
Je m’amuse dans le 7a/+, je me casse les dents sur des cotations plus ardues quelques fois, mais au-delà demanderait une rigueur
et un entraînement que je ne suis pas sûr de vouloir m’infliger.

Je progresse moins mais ça reste rigolo.

## Quelques recos

Avant de vous laisser, je vous partage quelques trucs qui m’ont agréablement marqué en 2024. Promis, on va faire vite !

### Lecture

<div style="display: flex; gap: 12px">
    <img src="derniere_etoile.jpg" alt="La dernière étoile">
    <img src="premiere_loi.webp" alt="La première loi">
    <img src="premier_de_cordee.webp" alt="Premier de cordée">
</div>

_[La dernière étoile — Fabien Tarlet](https://www.babelio.com/livres/Tarlet-La-derniere-etoile/1113758)_

Un petit space-opera parodique, dans la lignée de Pratchett mais dans un univers qui reprend les codes de la SF.

_[La première loi, tome 1 — Joe Abercrombie](https://www.babelio.com/livres/Abercrombie-La-Premiere-loi-tome-1--Premier-sang/1422325)_

De la fantasy un peu sombre, des intrigues politiques et des personnages plutôt bien construits.

Ce premier tome de la trilogie sert de mise en place, on verra bien ce que le deuxième me réserve.

_[Premier de cordée — Roger Frison-Roche](https://www.babelio.com/livres/Frison-Roche-Premier-de-cordee/36843)_

Ce livre parlera à tout les passionnés d’escalade et / ou d’alpinisme. On est loin de la sécurité qu’on a aujourd’hui en falaise,
et on souffle fort pendant certains passages.

On suit récit d’un guide qui se reconstruit après un accident, en vivant au rythme de la montagne.

### Gaming

<div style="display: flex; gap: 12px">
    <img src="acc.jpg" alt="Assetto Corsa Competizione">
    <img src="rogue_trader.jpg" alt="Warhammer 40,000: Rogue Trader">
    <img src="cairn.jpg" alt="Cairn">
</div>

_[Assetto Corsa Competizione](https://store.steampowered.com/app/805550/Assetto_Corsa_Competizione/)_

C’est officiel, je suis tombé dans le SimRacing. C’est cool mais qu’est-ce que ça prend du temps !

Et un des jeux qui m’a donné envie d’y passer encore plus de temps, c’est Assetto Corsa Competizione.

Moins arcade qu’un Forza Horizon ou un F1 2024, pas de fioritures et juste de la conduite.

Parmi les petits trucs qui m’ont donné la banane cette année : une course d’endurance de 2h sur Paul Ricard,
et sans se vautrer.

_[Warhammer 40,000: Rogue Trader](https://store.steampowered.com/app/2186680/Warhammer_40000_Rogue_Trader/)_

Un petit cRPG de Owlcat sur une licence bien connue (enfin pas par moi mais ça viendra peut-être un jour).

Pas de grosse révolution, mais le gameplay est pas mal et ça fait passer le temps sans trop se prendre la tête.

_[Cairn](https://store.steampowered.com/app/1588550/Cairn/)_

> Quoi ? Encore de l’escalade ?!

Forcément, quand on me vend un jeu sur de la grimpe, j’accours. J’avais joué à [Jusant](https://store.steampowered.com/app/1977170/Jusant/)
que j’avais bien aimé, mais pas au point d’y rejouer.

Là, la proposition se veut un peu plus immersive. Le jeu se prend en main assez rapidement et on souffle fort à certains passages.

Pour le moment, seule une démo est disponible. Démo sur laquelle j’ai passé au moins deux heures, à regrimper certains
pans en emprunant d’autres passages.

Après cette mise en bouche, on finit sur un plan large du sommet qu’on devra gravir dans le jeu final et ça donne
sacrément envie.

Hâte de faire du free solo depuis mon canap’.

### Podcasts

Comment ne pas terminer ces recos par mes deux podcasts chouchous, qui ont rythmés mes semaines au fil de l’année :
[le Floodcast](https://shows.acast.com/floodcast) et [Torréfaction](https://www.geekzone.fr/torre/).

## En conclusion

Bonne année 2025 🎉

<div style="width: 50%; margin: auto">

![wee-hee](wee-hee.gif)

</div>
