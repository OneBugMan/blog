+++
title = "Une année d'entretiens"
date = "2024-02-26"
+++

Mes trois dernières années professionnelles ont été un peu mouvementées.

Trouver une nouvelle opportunité après 4 ans dans une même boîte,
quitter une boîte au bout de trois mois, car trop de désaccords,
vouloir quitter un nouveau poste en raison de l'inexpérience technique de l'équipe qui gérait le projet ;
à plusieurs reprises, je me suis frotté au marché de l'emploi.

Et au fur et à mesures des entretiens, ma passion pour ce métier que j'exerce depuis bientôt 10 ans a laissé place à
un certain cynisme regrettable.

Maintenant que j'ai trouvé une nouvelle opportunité qui, espérons, correspond à ce que je recherchais - même si moins excitante sur le papier que les levées indécentes de l'écosystème startup parisien -
j'ai voulu en profiter pour faire une petite synthèse de ce que j'ai pu voir après été dans plusieurs dizaines de process de recrutements.

L'objectif ici n'est pas tant de (trop) râler pour râler, que de faire un retour d'expérience sur cette longue période de recherche. Ou juste râler.

## Mon parcours

Non, mon rêve n'a jamais été de passer mon temps à faire du CRUD.

Je voulais devenir vétérinaire, mais étant une quiche en SVT, j'ai vite dévié vers les maths et la physique.

Direction la case prépa et école d'ingénieur, mes premiers pas dans la programmation se font
avec des cours de C et de Java. A côté de ça, je découvre [Processing](https://processing.org/), un sketchbook de code créatif.
Je passe mon temps à coder des idées sans grands intérêts, loin des préoccupations que j'aurai une fois baigné dans le monde professionnel.

Au moment de choisir ma spécialisation, je me rends compte que contrairement à toutes les autres matières
dans lesquelles je suis (de nouveau) une quiche complète (coucou la mécanique des fluides), je peux passer mes soirées
entières à coder sans que ça me coûte. Filière informatique ce sera.

Ensuite, ce sera un stage de fin d'étude dans une ESN que je ne citerai pas, un premier job dans une autre ESN que je ne citerai pas non plus
et quelques années dans une startup.

### Mes premiers entretiens

En 7 ans, j'ai passé des entretiens pour seulement 2 boîtes (si on ne compte pas ceux que j'ai passé pour les missions d'ESN).

Pour la première, le process était somme toute classique : un entretien d'algorithmie, un entretien sur JavaScript et
un entretien orienté applicatif sur un framework JS (Angular.js à l'époque).
Tout s'est enchaîné en une après-midi dans les bureaux de la boîte en question.

La deuxième est un cas un peu particulier. Le CTO étant une connaissance de connaissance, ayant des affinités avec un certain langage académique
pour lequel j'avais les mêmes affinités, les discussions se sont faites de fil en aiguille. Il a pu voir un aperçu du code que je pondais
pour des projets perso sur GitHub à l'époque et m'a proposé de rejoindre la boîte qu'il avait lui-même rejointe quelques mois auparavant.

Pour la petite histoire, le premier entretien s'est déroulé autour d'une bière pendant lequel les discussions portaient sur l'état du projet
en toute transparence et la direction technique que ça prendrait au fil des mois.

S'ajoute à cela une rencontre avec les fondateurs, bien évidemment.

Avec le recul, ce sera sûrement l'entretien qui me marquera le plus, positivement.
Pas de faux-semblant, juste un constat honnête de la situation et du reste à faire. Quid de mes compétences techniques ?
Toutes les idées farfelues que j'avais étaient disponibles publiquement, avec une implémentation qui, aujourd'hui,
me ferait grincer des dents (heureusement, j'ai beaucoup appris entre temps).

Je sais ce que vous me direz :

> T'es gentil, mais toutes les boîtes ne peuvent pas se permettre de faire ce genre de choses. Surtout quand tu as déjà une équipe de 50 devs.

Certes. C'était l'avantage à être un des premiers recrutements.
Et l'avenir m'a montré que même une boîte de 5 personnes peuvent avoir des process de recrutements pétés.

Tout ce petit aparté pour dire qu'au final, j'ai eu de la chance dans mon début de carrière et que je n'ai pas
eu à développer une expertise sur comment passer les entretiens.

## Il serait peut-être temps de changer de crèmerie

### L'état du marché

Vouloir changer, c'est bien. Encore faut-il savoir pour quoi.

Direction certains job-boards pour un premier constat déprimant : peu sont les boîtes
qui proposent une vraie composante métier, encore moins sur un domaine intéressant.

Sans aucun chiffre à l'appui, la majorité des offres que je vois passer sont pour des ESN
ou structures de consulting.

D'autres proposent, victimes des cycles de mode dans le milieu, des postes sur des
langages qui ne m'attirent pas.

Sur ce point, lucide sur le fait que l'opportunité de retravailler au quotidien sur le
langage en H ne se représentera sans doute pas de sitôt, j'élargis mes critères.
Après tout, le langage importe peu (dans une certaine mesure) par rapport au contexte humain.
Je préfère encore travailler dans un environnement sain, sur une techno un peu moins
excitante que l'inverse.

Heureusement, le monde des startups fourmille de nouvelles idées et je trouve quelques
opportunités, pour des boîtes en phase de structuration (i.e. pas 4 devs en train de hacker
dans un appart en plein Paris). Je cible très vite des boîtes dans le domaine bancaire, de l'assurance
ou encore de la logistique.

Lors de ma deuxième phase de recherche, quelques années plus tard, le constat de base est le même.
A ceci près que le cliché du solutionnisme technologique est de plus en plus présent.
Or, quand on a travaillé un certain temps dans cette branche, on a un poil plus de mal à partager
l'optimisme de fondateurs qui expliquent comment balancer une plâtrée de micro-services réglera
des problèmes d'un domaine vétuste.
À ça, s'ajoutent des boîtes plus orientées "données", que ce soit à des fins purement marketing
ou de l'analytics.
Des années plus tard donc, je peine à trouver un projet qui m'emballe réellement.

### Le mot en T

J'ai commencé à chercher un nouveau job en plein COVID. Le télétravail était une norme forcée.

Deux ans après, le bât blesse.

Je ne vais pas faire une redite de ce qui a été dit moulte fois. Le télétravail est en recul,
et pouvoir travailler de chez soi sans des contraintes absurdes réduit grandement le nombre
de possibilités.

Alors oui, bosser chacun chez soi, ce n'est pas pour tout le monde.
Pour autant, tout le monde n'a pas un appartement en plein Paris ou ne veut pas se prendre
45 minutes (voire plus d'une heure) de trajet pour se rendre dans un open-space bruyant
trois fois par semaines (don't @ me les influenceurs LinkedIn).

Ce qui m'a doucement fait rire à chaque fois qu'on a abordé le sujet en entretien, ce sont les justifications.
Parce que oui, on n'autorise plus le travail de chez soi, mais on a de "bonnes" raisons.

Aussi, sur la roulette des excuses pétées, on pourra y lire entre autres :
- c'est important pour la cohésion d'équipe. C'est compliqué quand on ne voit pas les gens en direct.
- certains clients ont des besoins particuliers. Il faut qu'on puisse discuter de solutions de manières efficaces et rapides.

Mention spéciale au CTO qui m'explique qu'actuellement, quelques devs bossent de chez eux, mais lui veut revenir à un modèle hybride
et privilégier les recrutements parisiens. On en fait quoi du coup des personnes en remote ?

Il est vrai que faire venir les gens deux ou trois fois par semaine sur site contrebalance efficacement
un management dysfonctionnel, c'est bien connu.

C'est aussi vrai qu'on n'a aucun moyen de communiquer efficacement en ligne, que ce soit par écrit, audio ou vidéo.

Les discussions avec les quelques boîtes qui ne s'embêtent pas de ce genre de contraintes sont rafraîchissantes, bien
qu'on enfonce quelques portes ouvertes.
Ces boîtes responsabilisent ses équipes, tout simplement.

Et, en guise d'alternative, pour des boîtes qui se trouvent souvent hors région parisienne, le rythme est de rassembler
toutes les équipes une fois par mois ou tous les deux mois. Ce qui est un juste milieu raisonnable.

### À la recherche du temps perdu

Une fois le tour des annonces fait et les candidatures envoyées, commence les entretiens.
Et ça prend du temps.

Il faut compter entre 3 et 5 entretiens (voire plus pour des boîtes qui, étonnement, ne s'appellent
pas Google) :
- un entretien RH (30-45 minutes)
- un premier entretien technique (1h-1h30)
- un deuxième entretien technique (1h-1h30)
- une rencontre avec les équipes (1h)
- une rencontre potentielle avec les fondateurs (1h)

En tout, si tout se passe bien, on peut aller jusqu'à 5 heures pour une seule boîte.

C'est tout de suite plus, si l'entretien technique est un exercice à faire à la maison.

Et comme il vaut mieux éviter de miser sur un seul cheval, je postule pour plusieurs boîtes
en même temps.

Le tout sans poser le moindre jour de congé. Étant donné les déconvenues que peuvent donner
ces process, il vaut mieux passer ces entretiens en dehors des heures de boulot plutôt que
de réduire son solde de RTT.

Tout ça prend un temps fou, et surtout une énergie folle. Surtout quand le premier entretien
se déroule quasiment toujours de la même manière :
- présentation de son parcours
- présentation de la boîte
- pourquoi tu veux venir bosser chez nous
- voici le parcours du combattant qu'on te propose

L'avantage de l'expérience, c'est qu'on peut vite sentir quand tout ça n'en vaudra pas la
chandelle et s'épargner quelques heures laborieuses.

Nouveauté de cette année tout de même : les RHs qui se barrent en plein milieu du process
sans donner signe de vie. Ce qui m'est arrivé par deux fois.

La première fois, les entretiens s'étaient relativement bien passés. J'attendais donc qu'on
programme le suivant.
Quelques semaines se passent sans aucunes nouvelles. Les boîtes qui jouent aux abonnés absents, ça
existe et je me fais une raison.
On revient quelque temps après, en m'expliquant que la personne en charge du recrutement a dû quitter
la boîte et n'avait pas tout transmis. Chouette.

La deuxième fois, scénario quasi identique. Plusieurs semaines après, une personne freelance débauchée
pour pallier la démission de celle en charge de mon recrutement a repris le dossier.
Est-ce que cette dernière savait qu'elle allait quitter la boîte au moment de nos échanges ? Sans aucun doute.
Et pourtant, je me retrouve à réexpliquer à la nouvelle recrue où on en était et ce qu'il restait à faire.

À ce jour, c'est certainement le meilleur moyen de dissuader une personne de vous rejoindre.

Et je n'ai pas encore parlé des exercices à faire à la maison ou des entretiens qui se déroulent
sur une journée entière. Pour les derniers, c'est évidemment difficile à caser sans prendre
de jour de congés. Pour les deux, on frôle le foutage de gueule en termes de respect du temps
du candidat, bien que l'idée derrière soit compréhensible.

Dans le premier cas, les consignes donnent une fourchette de temps recommandée. Bien sûr, je n'ai quasiment
jamais respecté ça.

Quatre heures pour un serveur TCP agrémenté d'un interpréteur de commande (et donc du parsing) et des tests qui
couvrent tout ? Bien sûr.

Mention spéciale pour l'exercice (conçu pour un poste frontend alors que je postulais sur du backend)
qui se résumait à : "prends ce dataset et fais des trucs intéressants avec".
Une recette miracle pour pourrir votre lundi soir pour lequel vous aviez prévu autre chose.

Dans le deuxième cas, l'investissement d'une journée entière se solde par :

- un refus par mail sans aucun retour ni pédagogie concernant mon rendu et ce qui a pêché
- un retour, en tout et pour tout, de 5 minutes sur deux points vagues qui auraient pu être
    largement mis en exergue par un entretien plus court

C'est cool de se dire que mettre en situation la personne
(et accessoirement lui faire rencontrer la moitié de la boîte au fil de la journée) est une bonne mesure
de ce qu'elle vaut.

Sauf qu'après quatre heures déjà passées, les semaines précédentes, se faire refouler
de manière aussi peu justifiée au bout d'une journée entière est légèrement compliqué à accepter en gardant
son calme.

La conclusion, évidente : les entretiens sont devenus un parcours du combattant qui drainent votre temps,
votre énergie et votre passion pour ce job (en tout cas, en ce qui me concerne).

Qui plus est, ça n'a jamais été une garantie d'un bon fonctionnement en interne. Donc, que penser après avoir subi
tout ça si c'est pour se retrouver avec une boîte complètement dysfonctionnelle et aux pratiques techniques beaucoup
moins rigoureuses que ce qu'on vous fait miroiter en entretien ?

Parmi les dernières boîtes que j'ai contactées, j'ai soupiré quand le recruteur m'a expliqué toutes les étapes
pourtant habituelles. Puis j'ai coupé court, préférant passer mon temps à faire autre chose qu'à résoudre
un puzzle tordu et me vendre.

### Est-ce que les boîtes rêvent de moutons à 5 pattes ?

On passe notre temps à développer, il est normal de parler technique et coder un peu quand on postule pour une boîte.

Et puis vient le mur des gens qui prennent tout au pied de la lettre.

> Alors, t'as 4 ans d'expérience sur React mais on recherche une personne avec 5 ans ou plus.

> Désolé, on voit que tu n'es pas assez calé en DDD, TDD et autre *DD
    vu que les réponses que tu as données ne sont pas exactement celles qu'on a données à la RH.

Je paraphrase mais l'idée est là. On considère le nombre d'années d'expérience comme un seuil immuable qui servirait
de proxy à la qualité de la personne. Je vous laisserai chercher les memes de personnes qui demandent un nombre d'années
d'expérience plus grand que celui d'existence de la techno.

Avec le recul, c'est assez amusant de se prendre ce genre de refus alors que j'ai commencé à toucher à React de mon côté
dès que la librairie était sortie.

Ensuite, viens l'entre soi dans lequel il est compliqué de s'y faire une place quand on manque soi-même de conviction.
Quoi dire à des gens qui vont religieusement écrire tous leurs tests avant leur implémentation, vont faire de l'architecture hexagonale
ou vous bassiner avec du Domain Driven Development ?

Evidemment, avec le temps je me suis familiarisé avec tout ça en essayant d'en comprendre la motivation et le gain.

Là où ça devient compliqué, c'est de tempérer les méthodologies avec le contexte. Le "ça dépend" plaît moyennement.
On veut des gens sûrs d'eux, absolus dans leurs décisions.

Or, c'est compliqué sur certains points d'être sûr de soi. Tout ce qui est DDD, architecture hexagonale, je ne l'ai vu dans
aucun contexte professionnel. Est-ce qu'on a appliqué certains points intuitivement sans mettre de mot dessus ?
Probablement.
Reste à savoir si en fin de compte, il ne s'agit là que d'apprendre par coeur un vocabulaire d'initié plus que de vouloir
résoudre des problèmes techniques en équipe.

J'ai passé certains entretiens où la prémisse était une équipe qui expérimentait une nouvelle architecture et,
n'était même pas capable de m'aiguiller avec certitude sur certains de mes choix.

Viennent ensuite les entretiens un peu plus classique. Des séances en visio sur soit des problèmes
d'algorithmie, soit des problèmes pratiques.

Des premiers entretiens de ce type que j'ai passés en début de carrière, une chose m'est restée :

> C'est un entretien technique, mais c'est aussi l'occasion d'échanger et d'en savoir un peu plus sur la boîte, et réciproquement.

L'entretien technique, je le vois comme un moment qui doit me permettre de répondre à : "est-ce que j'ai envie de bosser avec cette personne au quotidien ?".
Peut-être à tort.

Dans la majorité des cas, cette heure est passée à résoudre un puzzle, souvent déconnecté de la réalité
business, fomenté par une équipe qui ne cherchera pas à en savoir plus sur vous.

Aussi, j'ai pu passer des entretiens où la personne s'est mise en sourdine pendant que je
codais sur un exercice bateau. Pour finir sur un : "euh, t'as des questions sur la boîte ?".

Bonjour, au revoir. Peut-être à bientôt.

Autant dire que ce genre de moment de solitude, qui semble être une corvée, ne constitue pas
une vitrine très reluisante.

Et quand je parle d'exercice déconnecté de la réalité, le champ des possibles est infini.
Quel est l'intérêt de se taper un algo de flood-filling dans le secteur bancaire ?
Quid de refaire un tri distribué à la Spark parce que le CTO est hyper fan, si c'est pour faire
du dashboard ? Et potentiellement se placer dans un écosystème déjà installé dans la boîte ?

Tout ce que ces exercices mettent de côté, c'est le contexte technique de la boîte.

On a vu que je me débrouillais pas trop mal pour faire une TODO list (même si c'est limite,
et que tu aurais pu utiliser un reducer au lieu d'un state). Super.

Premier jour dans une boîte ? La stack est déjà là, elle a potentiellement plusieurs années
et possède son lot de fantaisies propre à la boîte.

D'autres s'en accommoderont, je n'en doute pas.

Des entretiens techniques que j'ai pu faire, j'en retiens quelques-uns qui, étant là pour
tester mes capacités, donnent un aperçu des problèmes que l'équipe est amené à résoudre au
quotidien.

Et c'est aussi bête que de se dire : "on a un fichier texte un peu crade, on aimerait que
ça colle un peu plus à notre modèle de données. Comment tu t'y prendrais ?".

La discussion peut commencer, et ne s'arrête pas à un bête exercice de pensée, mais s'ouvre
sur les problèmes que les gens ont pu avoir sur certaines intégrations et comment ils s'y sont
pris.

Du pire que j'ai expérimenté de ce côté-là, il y a eu deux situations.

La première, une boîte avec une stack intéressante, mais en peine de trouver des profils.
Pourtant, l'objectif est de créer une petite famille (hmmm) et d'arrêter de se reposer sur
des freelances.

Le premier entretien technique ? Une suite de problème d'algo à faire en 30 minutes. Le chronomètre
démarre dès que la base de code est téléchargée et doit être poussée sur un repo avant l'échéance dés
30 minutes.

Je me foire sur certaines parties. Mais ayant l'habitude du mantra "c'est ton cheminement qui nous intéresse,
plus que le résultat final", je me demande quelle tournure la suite prendra.
Il ne m'a fallu pas attendre très longtemps, puisque quelques heures plus tard, je reçois ce qui semble être
un mail automatique de refus.

Le test était automatisé de bout en bout et personne, à aucun moment, n'a regardé ce que j'ai produit.
Ce que j'essaie d'expliquer à la recruteuse qui me répond simplement avec le rapport de tests.

La conclusion, devenue habituelle maintenant : tant mieux, cette boîte ne doit pas être faite pour moi.

La deuxième, un entretien censé être sur du React. Comme c'est la loterie à chaque fois,
je préfère ne pas trop me prendre la tête en avance de phase.

Petite question pour la mise en jambe :

> Il est 15h15. On aimerait connaître l'angle entre les deux aiguilles de l'horloge.

Sur l'échelle du malaise, on aurait pu y ajouter d'autres barreaux.

Alors oui, le problème n'est pas compliqué et j'aurais pu trouver la réponse si j'étais dans un meilleur jour.
Et une petite part d'orgueil a potentiellement éreinté ma patience.

Au bout de quelques minutes de réflexion laborieuse, une épiphanie. Cette question est digne d'un quiz
en ligne et je ne comprends pas comment on peut prendre une heure à quelqu'un pour commencer un test comme ça.

J'explique, un peu agacé, à la personne en face mon point de vue qui me répond en balbutiant, que
le business implique souvent des calculs et c'est une manière de voir comment les gens réfléchissent.

Sur du SaaS. Et j'ai bossé largement plus d'un an sur des problèmatiques de pricing.

Je considère encore aujourd'hui (peut-être à tort) que ce genre de questions se résume à du foutage de gueule, totalement
déconnecté du réel et qu'il serait de bon ton de proposer des problématiques business vraisemblables
plutôt que d'utiliser des proxys merdiques, pensés probablement comme ludiques dans la tête de certains.

Et quand je parle de problématiques vraisemblables, je ne parle pas de demander à une personne
de penser l'architecture principale du business en une heure.

Je mettrai sans doute ce point sur mon incapacité à faire du "design system" de manière rigoureuse
et efficace.

Pour autant, j'ai du mal à concevoir qu'on puisse te dire :
"bon, prends ce CSV dégueulasse. On voudrait avoir cette feature. Comment tu design ta solution ?"

Et la feature, ce n'est pas une petite brique à côté légèrement simplifiée pour les besoins de l'exo.
C'est le coeur de métier de la boîte, et le CSV est une vraie entrée. Le tout en une heure.

Une heure à essayer de démystifier un fichier de base à la qualité douteuse, et improviser une archi
d'un système sur lequel des personnes travaillent depuis plus d'un an.
Alerte spoiler : ça ne s'est pas bien passé.

Si j'étais totalement nouveau dans le secteur, je prendrais ça comme un signe que je ne suis pas fait pour ça.

J'ai bossé sur des systèmes en partant d'une page blanche, en essayant d'architecturer ça en pensant au moyen terme (pour le meilleur et pour le pire).
La dernière fois que j'ai eu à faire ça, j'ai mis sans doute plus de deux semaines avant de pondre la moindre ligne de code.

Et le projet vient se casser les dents sur la réalité, des erreurs bêtes d'anticipation ou de sous-estimation de certaines parties
viennent remettre en cause nos pré-conceptions. On itère, on apprend et on passe à autre chose.
Le tout prend un poil plus qu'une heure.

Donc l'idée de "bon, on a une heure, ponds-moi une solution pour notre business", c'est aussi oublier que la solution
en question a pris du temps, s'est mangé des murs et a évolué.


### Les cabinets de recrutements à la rescousse

Un point inédit dans ma carrière, je me suis essayé aux cabinets de recrutement.

Sur le papier, être en contact avec des gens qui sont au fait du marché et en mesure
de proposer les postes adaptés en fonction de mon profil est prometteur.

Fini les soirées à consulter WTTJ dans l'espoir de trouver une offre qui sorte du lot.

Dans la réalité, j'ai rapidement déchanté.

La première prise de contact se passe plutôt bien, mais la vitesse à laquelle tout se déroule
me donne un peu le vertige. Soit, on fait avec.

En l'espace de quelques jours, 3-4 appels sans prévenir. "Voici le topo du poste, est-ce que
ça t'intéresse ? Oui, non ?". Je suis toujours en poste, et parfois, je me retrouve avec mon
téléphone en train de sonner alors que je suis en réunion.
Les premières fois, ça passe. Les fois d'après, c'est juste gonflant.

Quid des propositions ? La plus-value est moindre. J'aurais pu tout aussi bien trouver ces
offres moi-même dans la majorité des cas.

Tu finis par passer quelques entretiens, et le téléphone sonne peu de temps après.
Résumé de l'entretien, des questions posées, du ressenti et autres.

J'ai l'impression d'avoir en face de moi des commerciaux d'ESN, plus soucieux de leurs
commissions que de leur mission première.

Le pire, c'est quand il y a de la redite entre le client final et le cabinet que ce soit
pour planifier la suite ou pour les retours. La perte de temps et la gestion des intermédiaires
font monter la frustration.

D'autres vous laisse un peu plus d'air quant à votre organisation ce qui me convient un peu mieux.
La contrepartie, c'est qu'une personne de ce cabinet vous enverra un message toutes les semaines
pour savoir où vous en êtes, sans avoir quelque chose de pertinent à ajouter.

De cette petite expérience, j'en conclus sans trop les accabler que ce n'est pas pour moi.

## La conclusion

Si vous avez tenu jusqu'ici, merci de m'avoir lu ! Encore un peu de courage, on arrive à la fin.

J'ai hésité longuement avant d'écrire ce billet, probablement de peur de passer pour un mauvais perdant.

Autant je peux reconnaître mes lacunes, autant certains retours me font penser que j'aurais peut-être dû
passer mon temps à faire autre chose de plus constructif.

En une année à prospecter pour un nouveau job par intermittence, hormis la critique de la méthodologie en tant
que telle, un des points qui m'a le plus impacté est mon ressenti personnel.

Si autant de boîtes me disent non, que penser de mes capacités en tant que développeur ?

Pourquoi dans toutes mes expériences, des personnes me disent qu'elles re-travailleraient avec moi avec plaisir
alors qu'aux vues de tous ces entretiens, je suis à la ramasse techniquement ?

Est-ce que je ne devrais pas prendre le temps nécessaire pour me former à passer des
entretiens comme on répéterait un numéro de cirque ?

Difficile de ne pas tomber dans un pseudo syndrome de l'imposteur après tout ça. Et encore plus difficile de se
motiver à coder un peu le soir, quand on se dit "à quoi bon" et quand vos intérêts techniques divergent avec
ceux qui veulent faire du business.

Je ne sais pas ce que je peux dire quand on me demande de me vendre et qu'à côté de ça je demande "euh pourquoi
Mongo ? Ah, c'est parce que c'est la DB avec le CTO a le plus d'affinité".

Tout ce que je sais à l'heure actuelle, c'est que j'ai enfin trouvé une boîte qui a l'air de correspondre à ce que je cherchais, loin de l'ambiance start-up,
clôturant au passage une année d'entretiens avec ses hauts et ses bas.

Pas franchement hâte d'en repasser.
