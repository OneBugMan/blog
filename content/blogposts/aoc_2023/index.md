+++
title = "L'Advent of Code 2023"
date = "2023-12-27"
+++

Pour celles et ceux qui ne connaîtrait pas, l'[Advent of Code](https://adventofcode.com/) est, comme son nom le laisse sous-entendre, un calendrier de l'avent de "petits" challenges d'algorithmie plutôt chouette.

Depuis 2015, du 1er au 25 Décembre, le site propose un problème en deux parties à résoudre.

Chaque jour, un peu de storytelling en lien avec Noël, des lutins pas très doués, une présentation du problème à résoudre avec une entrée de taille raisonnable et une explication du raisonnement.

Généralement, le problème se scinde en deux parties avec le même fonctionnement&nbsp;:
- la réponse attendue est un entier positif
- un exemple d'input (relativement petit) est donné avec le raisonnement pas par pas pour trouver la réponse
- un fichier d'input de taille beaucoup plus conséquente vous est généré par le site, que vous devrez utiliser pour trouver la réponse attendue (ce qui va, évidemment, poser des soucis de scalabilité de votre solution)
- une fois la bonne réponse trouvée, rebelotte pour la deuxième partie

Cette deuxième partie est soit :
- un ajustement du parsing de l'entrée
- une mise à l'échelle plus importante, rendant toutes solutions "brute-force" trop longues à faire tourner, voire impossible

Le challenge ayant commencé en 2015, je m'étais essayé à l'exercice de manière un peu laborieuse en 2017 sans grand succès (en tout et pour tout, 4 jours).

Cette année, à cause (ou grâce à, tout dépend de comment on voit le verre) de mon contexte professionnel un peu hasardeux, je me suis mis en tête d'essayer de faire cette édition 2023 de bout en bout.

Il faut noter que la difficulté des challenges peut varier grandement d'un jour à l'autre, les premiers jours étant des plus faciles.

## Et alors, le bilan Samuel&nbsp;?

### Hello Morty, I'm quiche-Rick

A ce jour, je viens de complèter les 25 jours (avec une aide considérable sur la dernière semaine mais j'y reviendrai).


L'AoC étant un challenge principalement DSA (pour Data Structure Algorithm), les quelques points à maîtriser sont, entre autre:
- le parsing, part léger du problème étant donné que le format est souvent simple et qu'il n'y a presque jamais de gestion d'erreur à gérer (l'input correspondant parfaitement au format présenté)
- le choix de structure de données
- les itérations, ou plus généralement l'algo que vous allez appliquer (duh, oui je sais)

Les premiers jours étaient relativement faciles, bien que certains étaient un peu plus corsés que d'autres.

Et après un mois de challenge, je me suis rendu compte que j'étais une quiche complète sur pas mal de points:
- les graphes
- la programmation dynamique
- tout ce qui est algo de floodfill, BFS etc... (bien que l'intuition soit là, je bloque complètement au moment d'écrire une ligne de code)
- algo de géomètrie pour calculer des aires de polygones, ou encore trouver les points à l'intérieur d'une courbe
- la gestion d'intervalles

Bref, que des choses un peu velues tant qu'on ne les a pas vu et / ou pratiquées maintes et maintes fois.

Evidemment, toutes ces choses ne me sont quasiment d'aucune utilité dans mon quotidien en tant que développeur web.

J'ai appris pas mal de choses, pour certaines dont j'ai un peu honte étant donné que ma carrière frôle bientôt la décennie :
- `deque` de Python (et d'autres structures comme les priority queues)
- le pathfinding, la traversée de graphes et d'autres algo de ce genre
- la [`Shoelace formula`](https://en.wikipedia.org/wiki/Shoelace_formula) et [`Pick's theorem`](https://en.wikipedia.org/wiki/Pick%27s_theorem)
- [`sympy`](https://www.sympy.org/en/index.html) pour faire de l'algèbre et [`networkx`](https://networkx.org/) pour la gestion complexe de graphe

Et de cette liste, j'en tire quelques sujets à approfondir dans les prochains mois, même si, encore une fois, ça ne me servira sans doute que très peu à faire du CRUD.

### Time is money, money is power, power is pizza, pizza is knowledge

_April Ludgate, Parks & Recs_

Tout ça, ça prend du temps. Encore plus lorsque vous cherchez par vous même (ce qui est évidemment le plus gratifiant).
Et, pour peu que vous coinciez mais qu'il vous faille vaquer à d'autres sujets un peu plus rentables, le problème vous restera dans la tête
et vous passerez une énergie folle à retourner le problème dans tout les sens et expérimenter entre deux builds.

Evidemment, si vous voulez être dans le leaderboard, il va falloir vous lever tôt (ce qui est loin d'être mon cas) et vous focaliser uniquement là-dessus (ce qui est, encore, loin d'être mon cas).
Pour le reste du communs des mortels, il faut relâcher un peu la pression. Résoudre un problème trois jours après n'est pas si dramatique que ça, et
qui plus est, vous éviterez d'y passer votre week-end.

Pour éviter de bloquer sans avancer dans votre coin, le subreddit [/r/adventofcode](https://www.reddit.com/r/adventofcode/) crée un megathread par problème
où tout le monde poste leurs idées de solutions. Comme l'input donné à chaque exercice diffère pour chaque utilisateur, vous ne trouverez pas de réponse directe (et c'est bien mieux comme ça).
Toutefois, ça donne quelques lignes directrices sur des algos, des astuces que vous ne connaîtriez pas.

Après avoir bloqué sur quelques problèmes de la dernière semaine, j'ai un peu changé d'approche quant au challenge.

Je suis tombé sur la très chouette chaîne [HyperNeutrino](https://www.youtube.com/@hyper-neutrino), qui a sorti chaque jour une vidéo résumant
sa solution au problème du jour, le tout saupoudré de quelques explications et illustrations [Excalidraw](https://excalidraw.com/).
Au lieu de me torturer chaque jour de manière désespérée, je me suis bloqué une petite heure à coder en ayant comme support ses vidéos.
Je peux comprendre l'approche, me noter quelques points à approfondir et exercer, et passer à autre chose.


### Une petite digression sur Copilot

Loin de moi l'idée d'en rajouter sur l'effervescence légèrement agaçante autour des LLMs, ni les doutes légitimes qu'on puisse avoir
sur les méthodes d'entraînement de l'outil sorti par Github.

Au bout d'un an à avoir lu les retours d'expérience et les délires futuristes de certains, j'ai décidé d'appliquer l'adage de mes parents&nbsp;:

> Bah comment tu peux savoir que c'est pas bon si t'as pas goûté

Si ça a fonctionné pour les endives au jambon, ça doit fonctionner pour d'autres trucs.

Un matin, j'ai donc craqué et pris une licence personnel pour Copilot. Et après une petite heure, la complétion fonctionnait
à merveille dans mon éditeur préféré.

Les premières semaines d'utilisation ont été plutôt agréables, les propositions m'ont fait gagné pas mal de temps sur la base de code
sur laquelle je travaille au quotidien. Les tests sont un poil moins rébarbatifs à écrire, et je gagne un peu de temps sur l'implémentation
de courtes fonctions utilitaires.

Mais sur l'AoC, le nombre de fois où j'ai levé les yeux au ciel ferait le bonheur de mon orthoptiste.
Evidemment, comme beaucoup de gens ont versionné leurs solutions des années précédentes sur Github, Copilot se fait un malin plaisir
à me vomir des dizaines de lignes de code d'exercices d'années précédentes, tel un élève un peu trop enthousiaste qui lève la main en permanence pour dire
"moi je sais, moi je sais".

Je me suis retrouvé embarqué à faire du pair-programming virtuel avec un outil qui ne me laissait pas
une seule seconde pour réfléchir proprement ou expérimenter.

Etant donné ma configuration, le moyen le plus simple pour laisser mon cerveau respirer fut de couper tout bonnement la complétion.
Ce qui est légèrement dommage pour un outil dont le but premier est d'assister.

Je le réactiverai sûrement pour des projets pro ou même perso et ferai en sorte de pouvoir le couper ou l'activer en deux touches (merci Neovim).

Pour le moment, l'effet waouh s'est un peu estompé, mais l'avoir dans sa caisse à outil en complément d'un bon plugin LSP rend le quotidien un peu plus agréable.

## Un dernier mot pour conclure ?

Choucroute.

Et joyeuses fêtes à toutes et tous.

![Happy Holidays](happy_holidays.gif)
