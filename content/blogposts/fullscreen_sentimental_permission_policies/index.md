+++
title = "Fullscreen sentimental, Chrome et les permissions policy"
date = "2024-06-21"
+++

Commençons par un petit jeu : est-ce que vous saurez trouver la différence entre ces deux images :

<div style="display: flex; gap: 15px;">

![fullscreen disabled](fullscreen_disabled.png)

![fullscreen enabled](fullscreen_enabled.png)

</div>

Réponse : l'une des vidéos peut être jouée en plein écran, pas l'autre.

Et c'est ainsi que commence une petite séance de débuggage WTF comme on les aime.

## Le contexte

Pour une quelconque raison, vous souhaitez inclure une vidéo Youtube sur un de vos sites.

Une [recherche rapide](https://letmegooglethat.com/?q=embed+youtube+video) plus tard, vous tombez sur un [tutoriel](https://support.google.com/youtube/answer/171780?hl=fr)
plutôt direct.

Première étape : allez sur la [vidéo](https://www.youtube.com/watch?v=V_SNDGwwGFM) en question.

Deuxième étape : trouver le bouton "Partager", cliquer sur "Intégrer" et tada, vous avez un snippet prêt à être copié-collé :

```html
<iframe
    width="560"
    height="315"
    src="https://www.youtube.com/embed/V_SNDGwwGFM?si=gNNpKH3mpbzRnpV8"
    title="YouTube video player"
    frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    referrerpolicy="strict-origin-when-cross-origin"
    allowfullscreen></iframe>
```

Ce qui m’intéresse particulièrement dans ce snippet, c'est l'attribut `allowfullscreen`.

Comme son nom le laisse présumer, ça laisse la main au site qui embarque l'iframe d’autoriser ou non le plein écran (cf [la doc](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#allowfullscreen)).

Dans notre cas, ça devrait être suffisant. Ça devrait l’être n'est-ce pas ?

> padme_anaking_meme.jpg

Sur Firefox et sûrement d'autres navigateurs non basés sur Chromium, oui. Sur Chrome, la situation est moins évidente.

## Les permissions policy

La différence dans les deux exemples que j'ai partagés en début d'article ?

Le header `Permissions-Policy` ajouté par le serveur lorsqu'il nous sert notre page HTML (qui est pourtant la même dans les deux cas).

Dans un cas, on a :

```http
Permissions-Policy: fullscreen=()
```

Et dans l'autre :

```http
Permissions-Policy: fullscreen=(self "https://www.youtube-nocookie.com")
```

Dans le premier exemple, on bloque le plein écran pour toutes les iframes de la page et dans le deuxième,
on l’autorise uniquement pour des iframes chargeant des resources depuis le domaine `https://www.youtube-nocookie.com`.

Et par défaut (i.e. si le header n'est pas rempli), on ne bloque le plein écran sur aucun domaine.

Merveilleux.

Pour tester ce genre de comportement, une config nginx plutôt simple suffit :

```nginx
events {}

http {
    server {
        root /data/www/;

        location /no-permissions {
            try_files /index.html =404;
        }

        location /empty-permissions {
            add_header 'Permissions-Policy' 'fullscreen=()';

            try_files /index.html =404;
        }

        location /valid-permissions {
            add_header 'Permissions-Policy' 'fullscreen=(self "https://www.youtube-nocookie.com")';

            try_files /index.html =404;
        }
    }
}
```

À noter que ce mécanisme peut contrôler les permissions de plusieurs fonctionnalités du navigateur (géolocalisation, webcam, picture-in-picture, ...).

Si vous voulez en savoir plus, je ne peux que vous conseiller la documentation MDN sur [le sujet](https://developer.mozilla.org/en-US/docs/Web/HTTP/Permissions_Policy).

## En guise de conclusion

![screaming in a pillow](scream_pillow.gif)
